package com.biocrypt.core;

import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class Response {
    private final boolean success;
    private final String message;

    public Response(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}

class FingerprintRequestData {
    private final List<Fingerprint> fingerprints = new ArrayList<>();

    public List<Fingerprint> getFingerprints() {
        return fingerprints;
    }
}

class VerifyFingerprintsRequestData {
    private Fingerprint testFingerprintBase64;
    private List<Fingerprint> registeredFingerprints;

    public Fingerprint getTestFingerprintBase64() {
        return testFingerprintBase64;
    }

    public List<Fingerprint> getRegisteredFingerprints() {
        return registeredFingerprints;
    }
}

@RestController
public class FingerprintsController {
    public static final double CROP_RATIO = 0.16;
    public static final int K = 3;
    public static final int n = 6;

    @PostMapping("/api/fingerprints/register/{userId}")
    @CrossOrigin(origins = "*")
    public String registerFingerprints(@PathVariable String userId,
                                       @RequestBody FingerprintRequestData fingerprintRequestData) throws IOException {

        for (Fingerprint fingerprint : fingerprintRequestData.getFingerprints()) {
            if (K <= n) {
                List<BufferedImage> shares = fingerprint.shares(CROP_RATIO, K, n);
                try {
                    //write all shares now
                    System.out.println("\n Shares size " + shares.size());
                    Api.distributeShares(userId, fingerprint.getId(), shares);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            } else {
                return "K should be less than n";
            }
        }
        return "done";
    }

    @GetMapping("/api/fingerprints/merge/{userId}")
    @CrossOrigin(origins = "*")
    public List<String> mergeShares(@PathVariable String userId) throws IOException {
        List<ShareMapping> shareMappings = Api.getShareMappings(userId);

        List<String> mergedImages = new ArrayList<>();
        for (ShareMapping shareMapping : shareMappings) {
            String mergedImageBas64 = Utility.convertToBase64(shareMapping.getMergedImage());
//            mergedImages.add(new Fingerprint(shareMapping.getFingerprintId(), mergedImageBas64));
            mergedImages.add(mergedImageBas64);
        }
        return mergedImages;
    }

    @PostMapping("/api/fingerprints/verify/")
    @CrossOrigin(origins = "*")
    public Response verifyFingerprints(@RequestBody VerifyFingerprintsRequestData verifyFingerprintsRequestData)
            throws Exception {

        BufferedImage testImage = Utility.getMergedImageFromShares(
                verifyFingerprintsRequestData.getTestFingerprintBase64().shares(CROP_RATIO, K, n)
        );

        String inputFilePath = Url.INPUT_PATH + "input.png";
        ImageIO.write(testImage, "PNG", new File(inputFilePath));

        for (Fingerprint fingerprint : verifyFingerprintsRequestData.getRegisteredFingerprints()) {
            BufferedImage registeredImage = fingerprint.image();
            String outputFilePath = Url.OUTPUT_PATH + "output-" + fingerprint.getId() + ".png";
            ImageIO.write(registeredImage, "png", new File(outputFilePath));

            boolean match = FingerprintMatcher.match(inputFilePath, outputFilePath);
            if (match) {
                System.out.println("Matched with id: " + fingerprint.getId());
                return new Response(true, "Authentication Successful");
            } else {
                System.out.println("Not Matched with id: " + fingerprint.getId());
            }
        }
        return new Response(false, "Authentication Failed");
    }
}



