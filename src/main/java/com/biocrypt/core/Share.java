package com.biocrypt.core;

public class Share implements Comparable<Share> {
    private final String id;
    private final Integer number;
    private final String data;

    public Share() {
        this.id = "";
        this.number = 0;
        this.data = "";
    }

    public Share(String id, int number, String data) {
        this.id = id;
        this.number = number;
        this.data = data;
    }

    public int getNumber() {
        return number;
    }

    public String getData() {
        return data;
    }

    @Override
    public int compareTo(Share share) {
        return this.number.compareTo(share.number);
    }
}
