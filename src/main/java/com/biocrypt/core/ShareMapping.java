package com.biocrypt.core;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShareMapping {
    private final String fingerprintId;
    private final List<Share> shares;

    public ShareMapping() {
        this.fingerprintId = "";
        this.shares = new ArrayList<>();
    }

    public ShareMapping(String fingerprintId, List<Share> shares) {
        this.fingerprintId = fingerprintId;
        this.shares = shares;
    }

    public String getFingerprintId() {
        return fingerprintId;
    }

    public List<Share> getShares() {
        return shares;
    }

    public BufferedImage getMergedImage() throws IOException {
        List<BufferedImage> shareImages = new ArrayList<>();
        Collections.sort(shares);

        for(Share share: shares) {
            shareImages.add(Utility.convertToBufferedImage(share.getData()));
        }
        return Utility.getMergedImageFromShares(shareImages);
    }
}
